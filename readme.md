1. Wie wird das Projekt eingereicht (installiert)?
Zuerst muss man den ganzen Ordner kopieren. Danach öffnet man das Terminal und geht mittels "cd" -Befehl bis zum "app" verzeichnis. Dort muss man einmal "npm install" im Terminal eingeben.

2. Wie wird das Projekt ausgeführt?
Im Terminal "npm start" eingeben, nachdem Schritt 1 ausgeführt wurde. Falls man das Projekt wieder beendet möchte, begibt man sich nur in das Termin, drückt "CRTL + C" und gibt "J" als Antwort ein.

3. Über welche URL wird das Projekt erreicht?
Wenn der Expressserver läuft, kann man es im Browser über http://localhost:8080/ aufrufen.