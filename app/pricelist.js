export function getPrice(cart) {
    if (cart.itemName == "appenzeller") {
        cart.price = 2;
    } else if (cart.itemName == "eier") {
        cart.price = 4;
    } else if (cart.itemName == "icedtea") {
        cart.price = 2;
    } else if (cart.itemName == "kalbsbratwuerste") {
        cart.price = 3;
    } else if (cart.itemName == "krustenkranz") {
        cart.price = 4;
    } else if (cart.itemName == "nektarinen") {
        cart.price = 1;
    } else if (cart.itemName == "olivenoel") {
        cart.price = 2;
    } else if (cart.itemName == "senf") {
        cart.price = 2;
    } else if (cart.itemName == "tomaten") {
        cart.price = 2;
    } else if (cart.itemName == "vanille_glace") {
        cart.price = 4;
    }
}